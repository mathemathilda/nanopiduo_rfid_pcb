### There are several errors and inconveniences in this version of the pcb. 
DONE: the voltage divider should output a ratio of 1:3.1-3.2 , a good choice is R_1 = 1k, R_2 = 1k, R_6 = 2.2k resulting in 5.2V for the board, which is finee with  4.7V ~ 5.6V	
DONE two resistors are mislabelled, it would be a good idea to find a better voltage divider for the power circuitry than the one used here showing wrong labels
* two resistors are unlabelled
* the smd jumpers are too big, not easy to close by hand using only solder
* the nanopi was intended to be on the other side, although it might be good to keep the power circuitry on one side and the nanopi on the other.
* the labels of the connectors are missing or undescriptive 
LATER?: i would rather prefer to build a version based on the Blueberry Pi to get a more compact design.
DONE: there are far smaller and sufficiently powerful buck converters layouts out there, so i would like to replace the lm2596  with a tps562200.
DONE: mounting holes are missing !!!
* the footprint of the ethernet conector does not have platet holes/GND for the casing
* more status leds would be nicer for debugging maybe even RGB one "advanced human interfacing"
* up to now the buzzer does not work, but this could also be a software problem 
DONE: pullup resistor R5 is at least for our setup too small, 2.2K work better, the cable to the button is some 15m long in my case, so it might actually grab some radio signals.  a small capacitor should be a good idea. this should also  be done for the second exposed gpio pin.
DONE: expose more of the unused pins. connect the interupt pin of the mfrc522, to some gpio!
DONE: the s9014 can switch 500mA at 12 V even up to 50 V, so why actually use a relais?  lets try to make the relais optional, or in other words get the relais signal to a header, so the solenoid can be connected  there if no relais is used. 
DONE: at least go to a surface mount version the relais for example the TX TH series, stable up to 220V AC with the option to use the acual grid voltage.
* dont make  SPI standard, rather more i2c ports, since I2C has an actuually standard
* make the diodes a bit smaller theyre huge right now. also, shouldnt the fluback diodes have resistors in series, yes, i think that would be better, and the value is supposed to be some Vpeak/Icoil, so 12/0.2  = 60 ohm  according to https://electronics.stackexchange.com/questions/115857/flyback-diodes-and-relays
* the signal for the audio speaker needs a pulldown instead of a pullup, as it needs to be low on startup else the speaker draws a strange currentpeak when not set up properly yet and nanopi reboots endlessly.
* the VDD side of the relais could be default connected to ground in the deenergized state, could be easier to cut the line here, also ground plane connnects via the ground pin of the relais, making it harder isolateand use elsewise.
* some output jack for the audio, maybe a micro coaxial for the micro and a pin header for the amplified speaker signal.
* more pullup/down resistors, 
* etc...
